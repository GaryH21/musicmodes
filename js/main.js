$(function() {
    var modes=new Array(3);
    for (i=0; i <3; i++) {
        modes[i]=new Array(8);
    }
   
    var notes = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"];
    modes[0] = ["2","2","1","2","2","2","1"]; // Major
    modes[1] = ["2","1","2","2","2","1","2"]; // Dorian
    modes[2] = ["1","2","2","2","1","2","2"]; // Phrygian
	modes[3] = ["2","2","2","1","2","2","1"]; // Lydian
	modes[4] = ["2","2","1","2","2","1","2"]; // Mixolydian
    modes[5] = ["2","1","2","2","1","2","2"]; // Minor
	modes[6] = ["1","2","2","1","2","2","2"]; // Lochrian
	modes[7] = ["2","1","2","2","1","3","1"]; // Harmonic Minor

   
    go();
    $("#root").change(function() {go()});
    $("#mode").change(function() {go()});
   
    function go()
    {
        output = "<table class=\"table\"><tr><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th></tr><tr>";
        mode = $("#mode").val();
        var root = $("#root").val();
        var note = root-1;
        output = output+"<td>"+notes[note]+"</td>";
        for (var i = 0; i < 7; i++) {
            
            x = parseInt(modes[mode][i]);
            y = parseInt(note);
            z = x+y;
            
            if(z > 11) { z = z-12; }
        
            output = output+"<td>"+notes[z]+"</td>";
            note = z;
        }
   
        output = output+"</tr></table>";
        
        $("#output").html(output);
    }
    
});
