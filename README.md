# Music Modes

A simple JavaScript app I built as a reference for different modes of each key.

Can be used at http://audioate.com/tools/modes_view

Currently supports:
* Major
* Dorian
* Phrygian
* Lydian
* Mixolydian
* Aeolean (Minor)
* Lochrian
* Harminic Minor

Still to come:
* More modes (such as melodic minor)
* A description about each mode
